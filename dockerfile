##Imagen de la que parto
FROM node:boron
##Carpeta de la app
WORKDIR /miapp
##Copia archivos
ADD . /miapp
#Paquetes necesarios
RUN npm install
##Puerto que expone la app
VOLUME /datos
EXPOSE 3000
##Comando de inicio app
CMD ["npm", "start"]
