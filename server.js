console.log("Aquì funcionando con nodemon")
var usuariosJSON = require('./usuarios.json')
var movimientosJSON = require('./movimientos2.json') /*require es instanciar algo*/
var express = require('express') /*Tirando variable que referencia al componente express*/
var bodyparser = require('body-parser')
var jsonQuery = require('json-query')
var requestJson = require('request-json')

var app = express() /*se crea variable app*/
app.use(bodyparser.json())

app.get('/', function(req, res){
  res.send('Hola API')
})

/*Mejor tener versionado por si sufre cambios*/
app.get('/v1/movimientos', function(req, res){
  res.sendfile('movimientos1.json') /*La respuesta que va devolver cuando solicites movimientos*/
})

app.get('/v2/movimientos', function(req, res){
  res.send(movimientosJSON) /*La respuesta que va devolver cuando solicites movimientos*/
})

app.get('/v2/movimientos/:id', function(req, res){
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send('Hemos recibido su peticiòn de consulta del movimiento #' + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
}) /*forma para pasarle paràmetros a la URL*/

app.get('/v2/movimientosq', function(req,res){
  console.log(req.query)
  res.send("Recibido")
})

/*Los params se vuelven obligatorios, se recomienda cuando hay un solo paràmetro*/
app.get('/v2/movimientosp/:id/:nombre', function(req,res){
  console.log(req.params)
  res.send("Recibido")
})

app.post('/v2/movimientos', function(req, res){
//  console.log(req)
//  console.log(req.headers=['authorization'])
//  if(req.headers=['authorization']!=undefined)
//  {
    var nuevo = req.body
    nuevo.id = movimientosJSON.length + 1
    movimientosJSON.push(nuevo)
    res.send("Movimiento dado de alta")
//  }else{
//    res.send("No esta autorizado")
//  }

})

/*Prueba Ejercicio metodo PUT - editar movimiento*/
app.put('/v2/movimientos/:id', function(req,res){
  var cambios = req.body //datos que quiero cambiar
  var actual = movimientosJSON[req.params.id-1]
  if(cambios.importe != undefined)
  {
    actual.importe = cambios.importe
  }
  if(cambios.ciudad != undefined)
  {
    actual.ciudad = cambios.ciudad
  }
  res.send("Cambios realizados")
})

/*Prueba Ejercicio metodo DELETE - borrar movimiento*/
app.delete('/v2/movimientos/:id', function(req, res){
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe *(-1),
    "concepto": "Negativo del" + req.params.id
  })
  res.send("Movimiento anulado")
})

//Ejercicio Usuarios GET
app.get('/v2/usuarios/:id', function(req,res){
  console.log(req.params.id)
  res.send(usuariosJSON[req.params.id-1])
})

//Ejercicio Usuarios POST (LOGIN)
app.post('/v2/usuarios/login', function(req,res){
  var email = req.headers ['email']
  var password = req.headers ['password']
  var resultados = jsonQuery('[email=' + email + ']', {data:usuariosJSON})
  //console.log(resultados)
    if(resultados.value != null && resultados.value.password == password){
      usuariosJSON[resultados.value.Id-1].estado='logged'
      res.send('{"login":"OK"}')
  }else {
    res.send('{"login":"error"}')
  }
})

//Ejercicio Usuarios POST (LOGOUT)
app.post('/v2/usuarios/logout/:id', function(req,res){
  var id = req.params.id
  var usuario = usuariosJSON[id-1]
    if(usuario.estado == 'logged'){
      usuario.estado = 'logout'
      res.send('{"logout": "OK"}')
    }else {
      res.send('{"logout":"error"}')
    }
})

//API v3 conectada a MLAB
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techumxesm/collections"
var apiKey = "apiKey=_gnF6qYNlAQW5GI0nYDAJTh7UjAOjlWP"
var clienteMlab = requestJson.createClient(urlMlabRaiz+ "?" + apiKey)

app.get('/v3', function(req, res){
  clienteMlab.get('', function(err, resM, body){
    var colleccionesUsuario = []
    if(!err){
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system.indexes"){
          colleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }
      }
      res.send(colleccionesUsuario)
    }else {
      res.send(err)
    }
  })
})

//Regresa listado de usuarios
app.get('/v3/usuarios', function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?" + apiKey)
  clienteMlab.get('',function(err, resM, body){
    res.send(body)
  })
})

//Alta de un usuario
app.post('/v3/usuarios', function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})

//GET usuario en concreto
app.get('/v3/usuarios/:id', function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  clienteMlab.get('?q={"Id":' + req.params.id + '}&' + apiKey,
  function(err, resM, body){
    res.send(body)
  })
})

//Modificar usuarios
app.put('/v3/usuarios/:id', function(req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
    clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body){
      res.send(body)
    })
})

//Regresa listado de movimientos
app.get('/v3/movimientos', function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz+ "/movimientos?" + apiKey)
  clienteMlab.get('',function(err, resM, body){
    res.send(body)
  })
})

//GET movimiento en concreto
app.get('/v3/movimientos/:id', function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos")
  clienteMlab.get('?q={"idcuenta":' + req.params.id + '}&' + apiKey,
  function(err, resM, body){
    res.send(body)
  })
})

app.listen(3000); /*Escuche por algun puerto en especifico*/
console.log("Escuchando en el puerto 3000")
