var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')
var should = chai.should()

chai.use(chaiHttp) //configurar chai con el modulo https

describe('Tests de Conectividad', ()=>{
 it('Google funciona',(done)=> {
   chai.request('http://www.google.com.mx').get('/').end((err,res)=>{
    // console.log(res)
    res.should.have.status(200) //valida contra objeto res si tiene una propiedad have y su valor es 200
    done()
   })
 })
})

describe('Tests de Conectividad de la API v3 usuarios', ()=>{
 it('Raíz de la API contesta',(done)=> {
   chai.request('http://localhost:3000').get('/v3').end((err,res)=>{
    // console.log(res)
    res.should.have.status(200)
    done()
   })
 })
 it('Raíz de la API funciona',(done)=> {
   chai.request('http://localhost:3000').get('/v3').end((err,res)=>{
    // console.log(res)
    res.should.have.status(200)
    res.body.should.be.a('array')
    done()
   })
 })
 it('Raíz de la API devuelve 2 colecciones',(done)=> {
   chai.request('http://localhost:3000').get('/v3').end((err,res)=>{
    console.log(res.body)
    res.should.have.status(200)
    res.body.should.be.a('array')
    res.body.length.should.be.eql(2)
    done()
   })
 })
 it('Raíz de la API devuelve los objetos correctos',(done)=> {
   chai.request('http://localhost:3000').get('/v3').end((err,res)=>{
    //console.log(res.body)
    res.should.have.status(200)
    res.body.should.be.a('array')
    res.body.length.should.be.eql(2)
    for (var i = 0; i < res.body.length; i++) {
      res.body[i].should.have.property('recurso')
      res.body[i].should.have.property('url')
    }
    done()
   })
 })
})

describe('Tests de Conectividad de la API v3 movimientos', ()=>{
 it('Raíz de la API movimientos contesta',(done)=> {
   chai.request('http://localhost:3000').get('/v3/movimientos').end((err,res)=>{
    // console.log(res)
    res.should.have.status(200)
    done()
   })
 })
 /*/it('Raíz de la API devuelve los movimientos correctos',(done)=> {
   chai.request('http://localhost:3000').get('/v3/movimientos').end((err,res)=>{
    //console.log(res.body)
    res.should.have.status(200)
    res.body.should.be.a('array')
    done()
   })
 })*/
})
